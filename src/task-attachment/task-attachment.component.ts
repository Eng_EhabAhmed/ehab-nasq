import { Component,Input, OnInit } from '@angular/core';

@Component({
  selector: 'task-attachment',
  templateUrl: './task-attachment.component.html',
  styleUrls: ['./task-attachment.component.css']
})
export class TaskAttachmentComponent implements OnInit {

  @Input() attachment: number; 

  constructor() { }

  ngOnInit() {
  }

}
