import { Component,Input, OnInit } from '@angular/core';

@Component({
  selector: 'task-done',
  templateUrl: './task-done.component.html',
  styleUrls: ['./task-done.component.css']
})
export class TaskDoneComponent implements OnInit {

  @Input() doneClass: string; 
  constructor() { }

  ngOnInit() {
  }

}
