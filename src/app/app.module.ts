import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppHeaderComponent } from '../app-header/app-header.component';
import { DropDownComponent } from '../drop-down/drop-down.component';
import { FabComponent } from '../fab/fab.component';
import { TaskAttachmentComponent } from '../task-attachment/task-attachment.component';
import { TaskFollowersComponent } from '../task-followers/task-followers.component';
import { AddCardComponent } from '../add-card/add-card.component';
import { TaskImgComponent } from '../task-img/task-img.component';
import { TaskVideoComponent } from '../task-video/task-video.component';
import { TaskDoneComponent } from '../task-done/task-done.component';


import {DragDropModule} from '@angular/cdk/drag-drop';

@NgModule({
  declarations: [
    AppComponent,
    AppHeaderComponent,
    DropDownComponent,
    FabComponent,
    TaskAttachmentComponent,
    TaskFollowersComponent,
    AddCardComponent,
    TaskImgComponent,
    TaskVideoComponent,
    TaskDoneComponent,
  ],
  imports: [
    BrowserModule,
    DragDropModule
  ],
  exports:[DragDropModule],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: []
})
export class AppModule { }
