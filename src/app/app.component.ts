import { Component } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  protected boards  = [
    {
      id : 1,
      name : 'Design area',
      boardColor : 'purple-col',
      tasks : 
      [
        {
          id: 1,
          name : "Task Name",
          video : 
            {
              text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy textever since the 1500s",
              img : "assets/img/coffee.jpg",
              video : "assets/img/screenshot-app.zeplin.io-2018.11.21-16-06-46.png"
            },
          followers :
            [
              "assets/img/avatar_7bca99976f0d_128.jpg",
              "assets/img/220px-Cameronavatar.jpg",
              "assets/img/270200-6.jpg"
            ],
          attachment : null,
          image : null
        },
        {
          id: 2,
          name : "Task Name",
          video : null,
          followers :
            [
              "assets/img/avatar_7bca99976f0d_128.jpg",
              "assets/img/220px-Cameronavatar.jpg"
            ],
          attachment : 5,
          image : null
        }
      ]
    },
    {
      id : 2,
      name : 'Frontend area',
      boardColor : 'orange-col',
      tasks : 
      [
        {
          id: 3,
          name : "Task Name",
          video : null,
          followers :
            [
              "assets/img/avatar_7bca99976f0d_128.jpg",
              "assets/img/220px-Cameronavatar.jpg",
              "assets/img/270200-6.jpg",
              "assets/img/270200-6.jpg",
              "assets/img/nancy_wake24june15.jpg"
            ],
          attachment : 3,
          image : null
        },
        {
          id: 4,
          name : "Task Name",
          video : null,
          followers : null,
          attachment : 3,
          image : "assets/img/pexels-photo-814499.jpeg"
        }
      ]
    },
    {
      id : 3,
      name : 'Development area',
      boardColor : 'blue-col',
      tasks : 
      [        
        {
          id: 5,
          name : "Task Name",
          video : null,
          followers : null,
          attachment : 3,
          image : "assets/img/pexels-photo-814499.jpeg"
        },
        {
          id: 6,
          name : "Task Name",
          video : 
            {
              text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy textever since the 1500s",
              img : "assets/img/coffee.jpg",
              video : "assets/img/screenshot-app.zeplin.io-2018.11.21-16-06-46.png"
            },
          followers :
            [
              "assets/img/avatar_7bca99976f0d_128.jpg",
              "assets/img/220px-Cameronavatar.jpg",
              "assets/img/270200-6.jpg",
              "assets/img/nancy_wake24june15.jpg"
            ],
          attachment : null,
          image : null
        }
      ]
    },
    {
      id : 4,
      name : 'Board name',
      boardColor : 'green-col',
      tasks : 
      [
        {
          id: 7,
          name : "Task Name",
          video : null,
          followers :
            [
              "assets/img/avatar_7bca99976f0d_128.jpg",
              "assets/img/220px-Cameronavatar.jpg",
              "assets/img/270200-6.jpg",
              "assets/img/nancy_wake24june15.jpg"
            ],
          attachment : 3,
          image : null
        },
        {
          id: 8,
          name : "Task Name",
          video : null,
          followers : null,
          attachment : 3,
          image : "assets/img/pexels-photo-814499.jpeg"
        }
      ]
    }

  ]

  protected listIds = [];
  
  constructor() { 
    let index : number = 0;
    this.boards.forEach(x=>{
      this.listIds.push('cdk-drop-list-'+index);
      index++;
    })

  }

  drop(event: CdkDragDrop<string[]>) {
    console.log(event);
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }
}
