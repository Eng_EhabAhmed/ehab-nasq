import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'task-img',
  templateUrl: './task-img.component.html',
  styleUrls: ['./task-img.component.css']
})
export class TaskImgComponent implements OnInit {

  @Input() image: string; 
  constructor() { }

  ngOnInit() {
  }

}
