import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskImgComponent } from './task-img.component';

describe('TaskImgComponent', () => {
  let component: TaskImgComponent;
  let fixture: ComponentFixture<TaskImgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskImgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskImgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
