import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'task-video',
  templateUrl: './task-video.component.html',
  styleUrls: ['./task-video.component.css']
})
export class TaskVideoComponent implements OnInit {

  @Input() data: any; 
  constructor() { }

  ngOnInit() {
  }

}
