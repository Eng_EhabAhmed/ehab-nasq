import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'task-followers',
  templateUrl: './task-followers.component.html',
  styleUrls: ['./task-followers.component.css']
})
export class TaskFollowersComponent implements OnInit {

  @Input() followers: any; 

  constructor() { }

  ngOnInit() {
  }

}
